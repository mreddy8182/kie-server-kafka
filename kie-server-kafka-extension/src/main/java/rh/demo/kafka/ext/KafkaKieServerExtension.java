/*
 * Copyright 2016 Red Hat, Inc. and/or its affiliates.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package rh.demo.kafka.ext;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.function.Consumer;

import rh.demo.kafka.service.KafkaService;
import rh.demo.kafka.RecordHandler;
import rh.demo.kafka.SignalEvent;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.jbpm.services.api.ProcessService;
import org.kie.api.KieServices;
import org.kie.api.command.Command;
import org.kie.server.services.api.KieContainerInstance;
import org.kie.server.services.api.KieServerExtension;
import org.kie.server.services.api.KieServerRegistry;
import org.kie.server.services.api.SupportedTransports;
import org.kie.server.services.impl.KieServerImpl;
import org.kie.server.services.jbpm.JbpmKieServerExtension;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import rh.demo.kafka.service.KafkaServiceImpl;

/**
 * Adds capabilities to process Kafka event stream as signals to containers.
 */
public class KafkaKieServerExtension implements KieServerExtension {

    private static final Logger LOG = LoggerFactory.getLogger(KafkaKieServerExtension.class);

    private static final Boolean disabled = Boolean
            .parseBoolean(System.getProperty("org.kie.server.kafka.ext.disabled", "false"));

    private ProcessService processService;
    private KafkaService kafkaService;

    @Override
    public boolean isInitialized() {
        return false;
    }

    @Override
    public void init(KieServerImpl kieServer, KieServerRegistry registry) {
        KieServerExtension jbpmExtension = registry.getServerExtension(JbpmKieServerExtension.EXTENSION_NAME);
        if (jbpmExtension == null) {
            // no jbpm services, no kafka support
            LOG.warn("Disabling KafkaKieServerExtension due to missing dependency: {}",
                    JbpmKieServerExtension.EXTENSION_NAME);
            return;
        }

        processService = getProcessService(jbpmExtension);

        if (processService == null) {
            // no process service, no kafka support
            LOG.warn("Disabling KafkaKieServerExtension due to missing dependency: {}", ProcessService.class.getName());
            return;
        }

        kafkaService = new KafkaServiceImpl();
        ((KafkaServiceImpl)kafkaService).init();
    }

    private ProcessService getProcessService(KieServerExtension jbpmExtension) {
        ProcessService processService = null;
        for (Object service : jbpmExtension.getServices()) {
            if (service != null && ProcessService.class.isAssignableFrom(service.getClass())) {
                processService = (ProcessService) service;
                break;
            }
        }
        return processService;
    }

    @Override
    public boolean isActive() {
        return !disabled;
    }

    @Override
    public void destroy(KieServerImpl kieServer, KieServerRegistry registry) {
        kafkaService.shutdown();
    }

    @Override
    public String getImplementedCapability() {
        return "Kafka";
    }

    @Override
    public List<Object> getServices() {
        return Collections.singletonList(kafkaService);
    }

    @Override
    public String getExtensionName() {
        return "Kafka";
    }

    @Override
    public Integer getStartOrder() {
        return 10;
    }

    @Override
    public List<Object> getAppComponents(SupportedTransports type) {
        return Collections.emptyList();
    }

    @Override
    public <T> T getAppComponents(Class<T> serviceType) {
        return null;
    }

    @Override
    public void createContainer(String id, KieContainerInstance kieContainerInstance, Map<String, Object> parameters) {

        Properties containerProperties = new Properties();

        try (InputStream propsFile = kieContainerInstance.getKieContainer().getClassLoader()
                .getResourceAsStream("kafka.properties")) {
            if (propsFile != null) {
                containerProperties.load(propsFile);
            }
        } catch (IOException e) {
            LOG.warn("Could not load kafka.properties while creating container " + id, e);
        }

        String topicList = containerProperties.getProperty("topics");
        if (processService != null && topicList != null) {
            String[] topics = topicList.split(",");
            String recordHandlerClass = containerProperties.getProperty("recordHandler");
            try {
                Class<? extends RecordHandler> recordHandlerClazz = Class.forName(recordHandlerClass).asSubclass(RecordHandler.class);
                RecordHandler recordHandler = recordHandlerClazz.newInstance();
                kafkaService.registerTopicConsumer(id, Arrays.asList(topics), new TopicConsumer(processService, id, recordHandler));
            } catch (ClassNotFoundException | IllegalAccessException | InstantiationException | ClassCastException e) {
                LOG.warn("Could not instantiate RecordHandler while creating container " + id, e);
            }

        }
    }

    @Override
    public void updateContainer(String id, KieContainerInstance kieContainerInstance, Map<String, Object> parameters) {
        // deregister and re-register
        disposeContainer(id, kieContainerInstance, parameters);
        createContainer(id, kieContainerInstance, parameters);

    }

    @Override
    public boolean isUpdateContainerAllowed(String id, KieContainerInstance kieContainerInstance, Map<String, Object> parameters) {
        // update is always allowed
        return true;
    }

    @Override
    public void disposeContainer(String id, KieContainerInstance kieContainerInstance, Map<String, Object> parameters) {
        kafkaService.deregisterTopicConsumer(kieContainerInstance.getContainerId());
    }


    public static class TopicConsumer implements Consumer<ConsumerRecord> {

        ProcessService processService;
        String id;
        RecordHandler<?, ?> recordHandler;

        private TopicConsumer(ProcessService processService, String containerId, RecordHandler<?, ?> recordHandler) {
            this.processService = processService;
            this.id = containerId;
            this.recordHandler = recordHandler;
        }

        public void accept(ConsumerRecord record) {
            SignalEvent event = recordHandler.apply(record);
            Command command = KieServices.Factory.get().getCommands().newSignalEvent(event.getProcessInstanceId(), event.getEventType(), event.getEvent());

            processService.execute(id, command);
        }
    }

}
