package rh.demo.kafka;

public class SignalEvent {

    private long processInstanceId = -1;

    private String correlationKey = null;

    private String eventType;

    private Object event;

    public SignalEvent() {
    }

    public SignalEvent(String eventType,
                       Object event) {
        this(-1, eventType, event);
    }

    public SignalEvent(long processInstanceId,
                       String eventType,
                       Object event) {
        this.processInstanceId = processInstanceId;
        this.eventType = eventType;
        this.event = event;
    }

    public SignalEvent(String correlationKey,
                       String eventType,
                       Object event) {
        this.correlationKey = correlationKey;
        this.eventType = eventType;
        this.event = event;
    }

    public long getProcessInstanceId() {
        return processInstanceId;
    }

    public String getCorrelationKey() {
        return correlationKey;
    }

    public String getEventType() {
        return eventType;
    }

    public Object getEvent() {
        return event;
    }
}
