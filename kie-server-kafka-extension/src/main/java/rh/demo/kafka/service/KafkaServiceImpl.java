/*
 * Copyright 2016 David Murphy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package rh.demo.kafka.service;


import com.google.common.collect.ImmutableSetMultimap;
import com.google.common.collect.Multimaps;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.common.TopicPartition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.naming.InitialContext;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.*;
import java.util.function.Consumer;

import static com.google.common.base.Predicates.equalTo;
import static com.google.common.base.Predicates.not;

public class KafkaServiceImpl implements KafkaService {

    private static final Logger LOG = LoggerFactory.getLogger(KafkaServiceImpl.class);

    private static final Properties CONSUMER_CONFIG = new Properties();

    static {
        CONSUMER_CONFIG.put("bootstrap.servers", System.getProperty("kie.server.kafka.bootstrap.servers", "localhost:9092"));
        CONSUMER_CONFIG.put("group.id", System.getProperty("kie.server.kafka.group.id", "kie.server.kafka"));
        CONSUMER_CONFIG.put("enable.auto.commit", System.getProperty("kie.server.kafka.enable.auto.commit", "true"));
        CONSUMER_CONFIG.put("auto.commit.interval.ms", System.getProperty("kie.server.kafka.auto.commit.interval.ms", "1000"));
        CONSUMER_CONFIG.put("session.timeout.ms", System.getProperty("kie.server.kafka.session.timeout.ms", "30000"));
        CONSUMER_CONFIG.put("key.deserializer", System.getProperty("kie.server.kafka.key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer"));
        CONSUMER_CONFIG.put("value.deserializer", System.getProperty("kie.server.kafka.value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer"));
    }

    private KafkaConsumer<String, String> consumer;

    private ImmutableSetMultimap<String, String> topicMappings = ImmutableSetMultimap.of();
    private Map<String, Consumer<ConsumerRecord>> consumerIndex = new HashMap<>();

    private ConsumerPollingTask task;
    private Future<?> poll;

    private String threadFactoryLookup = System.getProperty("kie.server.kafka.thread.factory", "java:comp/DefaultManagedThreadFactory");
    private ExecutorService executor = null;

    public void init() {
        consumer = new KafkaConsumer<>(CONSUMER_CONFIG);

        ThreadFactory threadFactory;

        try {
            threadFactory = InitialContext.doLookup(threadFactoryLookup);
        } catch (Exception e) {
            LOG.debug("Thread factory lookup failed, using default", e);
            threadFactory = Executors.defaultThreadFactory();
        }

        executor = Executors.newSingleThreadExecutor(threadFactory);
    }

    @Override
    public void shutdown() {
        executor.shutdown();
        try {
            if (!executor.awaitTermination(60, TimeUnit.SECONDS)) {
                executor.shutdownNow();
            }
        } catch (InterruptedException e) {
            executor.shutdownNow();
            Thread.currentThread().interrupt();
        }
        consumer.close();
    }


    @Override
    public <T extends Consumer<ConsumerRecord>> void registerTopicConsumer(String consumerId, List<String> topics, T consumer) {
        topicMappings = ImmutableSetMultimap.<String, String>builder().putAll(topicMappings)
                .putAll(consumerId, topics).build();
        consumerIndex.put(consumerId, consumer);
        cycleConsumer();
    }

    @Override
    public void deregisterTopicConsumer(String consumerId) {
        topicMappings = ImmutableSetMultimap.copyOf(Multimaps.filterKeys(topicMappings, not(equalTo(consumerId))));
        consumerIndex.remove(consumerId);
        cycleConsumer();
    }

    @Override
    public Producer producerFor(String topic) {
        throw new UnsupportedOperationException("This is not implemented yet");
    }

    private void cycleConsumer() {
        // kill the polling job
        if(task != null && poll != null) {
            task.shutdown();
            try {
                poll.get();
            } catch (InterruptedException | ExecutionException e) {
                //no-op
            }
        }

        // update subscriptions
        consumer.subscribe(topicMappings.values());

        // start the polling job
        task = new ConsumerPollingTask();
        poll = executor.submit(task);

    }

    private class ConsumerPollingTask implements Runnable{
        volatile boolean shutdown = false;

        @Override
        public void run() {
            while (!shutdown) {
                ConsumerRecords<String, String> records = consumer.poll(500);
                for (TopicPartition partition : records.partitions()) {
                    for (ConsumerRecord<String, String> record : records.records(partition)) {
                        for (String consumerId : topicMappings.inverse().get(partition.topic())) {
                            consumerIndex.get(consumerId).accept(record);
                        }
                    }
                }
            }
        }

        void shutdown(){
            this.shutdown = true;
        }
    }
}
